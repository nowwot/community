# nowwot-community

Cloned from [official TelescopeJS sample project repo](https://github.com/TelescopeJS/sample-project)

## Updating

    $ meteor update

## Deploying

    $ modulus deploy -p nowwot-community

## Notes

* Currently no customizations done in source. Extra folders are just there for example when we wish to do customizations.
* Do not include any sensitive information in this repository.
* Ensure that MONGO_URL is set to the shared database.

## TODO:

* Automatic deployment based on git push
* Either store customization data in this repository or make customizations via custom packages.
